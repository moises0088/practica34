function cargarimagenes(imgBien, imgMal) {

	let imgBn = document.querySelector("#imagenBien");
	let imgMl = document.querySelector("#imagenMal");

	imgBn.style.backgroundImage = 'url("img/'+imgBien+'")';
	imgMl.style.backgroundImage = 'url("img/'+imgMal+'")';

}

function crearBarra () {
	let barra = document.querySelector("#barra");
	let apuntador;

	barra.style.width = "80%";

	setTimeout((e) => {
	  	clearInterval(apuntador);
	  	alert("has perdido");
	}, 81000)

	apuntador = setInterval((e) => {
	   barra.style.width = parseInt(barra.style.width)-1+"%";
	   console.log(e);
	}, 1000);
}

function colocarChecks() {
	let checks = document.querySelectorAll(".check");

	checks.forEach((check) => {

	   check.addEventListener("click",function (evento) {
	   		evento.target.style.opacity =  1;
	   		comprobar();
	   });
	})
}

function moverCheck(selector, posX, posY) {
	let miCheck = document.querySelector(selector);
	console.log(miCheck);
	console.log(posX);
	console.log(posY);
	miCheck.style.top = posX ;
	miCheck.style.left = posY;
}

function comprobar (argument) {
	let checks =  document.querySelectorAll(".check");
	let cont = 0;

	checks.forEach((check) => {
	  if(check.style.opacity == 1){
		cont++;
	  }
	});

	if(cont==5){
		alert("Has ganado");	
	}
}

window.addEventListener("load",function () {
	
	cargarimagenes("2bien.png","2mal.png");
	crearBarra();
	colocarChecks();

	moverCheck("#check1", "10px", "165px");
	moverCheck("#check2", "39px", "137px");
	moverCheck("#check3", "157px", "142px");
	moverCheck("#check4", "45px", "255px");
	moverCheck("#check5", "35px", "170px");
})